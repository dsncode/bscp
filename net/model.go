package net

import (
	"fmt"
	"io/ioutil"

	"golang.org/x/crypto/ssh"
)

// Credentials represents a user that has access to a remote server
type Credentials struct {
	Username       string
	privateKeyPath string
	Signer         ssh.Signer
}

// RemoteServerConfig represents the server we would like to connect to
type RemoteServerConfig struct {
	host string
	port int
}

// NewRemoteServerConfig creates a server config
func NewRemoteServerConfig(host string, port int) RemoteServerConfig {
	return RemoteServerConfig{
		host: host,
		port: port,
	}
}

// GetConnectionString returns a conn string
func (conf *RemoteServerConfig) GetConnectionString() string {
	return fmt.Sprint(conf.host, ":", conf.port)
}

// NewCredentialsFromPrivateKey creates a set of credentials to connect to a remote server
func NewCredentialsFromPrivateKey(username string, privateKeyPath string) (Credentials, error) {
	privateKey, err := ioutil.ReadFile(privateKeyPath)
	if err != nil {
		return Credentials{}, err
	}
	signer, err := ssh.ParsePrivateKey(privateKey)
	if err != nil {
		return Credentials{}, err
	}
	return Credentials{
		Signer:         signer,
		Username:       username,
		privateKeyPath: privateKeyPath,
	}, nil
}
