package bscp

import (
	"io"
	"log"
	"os"

	"github.com/pkg/sftp"
	"gitlab.com/dsncode/bscp/net"
	"golang.org/x/crypto/ssh"
)

// Client creates a client
type Client struct {
	conn      *ssh.Client
	sftclient *sftp.Client
}

// NewClient creates a xscp client
func NewClient(credentials net.Credentials, serverConfig net.RemoteServerConfig) (*Client, error) {

	config := &ssh.ClientConfig{
		User: credentials.Username,
		Auth: []ssh.AuthMethod{
			ssh.PublicKeys(credentials.Signer),
		},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}

	// connect
	conn, err := ssh.Dial("tcp", serverConfig.GetConnectionString(), config)
	if err != nil {
		return &Client{}, err
	}

	// create new SFTP client
	client, err := sftp.NewClient(conn)
	if err != nil {
		return &Client{}, err
	}

	return &Client{
		sftclient: client,
		conn:      conn,
	}, nil
}

// Close all connection
func (client *Client) Close() {
	client.conn.Close()
	client.sftclient.Close()
}

// Download copy a remote file to local machine
func (client *Client) Download(source string, target string) (int64, error) {

	// open source file
	srcFile, err := client.sftclient.Open(source)
	if err != nil {
		return 0, err
	}

	// create destination file
	dstFile, err := os.Create(target)
	if err != nil {
		return 0, err
	}
	defer dstFile.Close()

	// copy source file to destination file
	bytes, err := io.Copy(dstFile, srcFile)
	if err != nil {
		return 0, err
	}

	// flush in-memory copy
	err = dstFile.Sync()
	if err != nil {
		log.Fatal(err)
	}
	return bytes, nil
}

// Upload a file to remote server
func (client *Client) Upload(source string, target string) (int64, error) {
	// create destination file
	dstFile, err := client.sftclient.Create(source)
	if err != nil {
		log.Fatal(err)
	}
	defer dstFile.Close()

	// create source file
	srcFile, err := os.Open(source)
	if err != nil {
		return 0, err
	}

	// copy source file to destination file
	bytes, err := io.Copy(dstFile, srcFile)
	if err != nil {
		return 0, err
	}

	return bytes, nil
}
